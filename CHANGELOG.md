## [1.1.1](https://gitlab.com/ayedocloudsolutions/lifecycle/compare/v1.1.0...v1.1.1) (2021-05-10)


### Bug Fixes

* removed docker buildx ([a880f65](https://gitlab.com/ayedocloudsolutions/lifecycle/commit/a880f658e8f8af4f4a645008ea379fcb0beea90f))

# [1.1.0](https://gitlab.com/ayedocloudsolutions/lifecycle/compare/v1.0.3...v1.1.0) (2021-05-10)


### Features

* added experimental syntax to Dockerfile ([ebcbfd0](https://gitlab.com/ayedocloudsolutions/lifecycle/commit/ebcbfd028bc1d54bcb02cc9c224cfb211fac9c2d))

## [1.0.3](https://gitlab.com/ayedocloudsolutions/lifecycle/compare/v1.0.2...v1.0.3) (2021-05-10)


### Bug Fixes

* disabled TLS verify for docker ([9958623](https://gitlab.com/ayedocloudsolutions/lifecycle/commit/9958623959646afe05e28bd5f3b26a01f860c763))

## [1.0.2](https://gitlab.com/ayedocloudsolutions/lifecycle/compare/v1.0.1...v1.0.2) (2021-05-10)


### Bug Fixes

* added timeout to wait for docker in CI ([35c433b](https://gitlab.com/ayedocloudsolutions/lifecycle/commit/35c433b9a071c539a486cadab1e37223300f4d91))

## [1.0.1](https://gitlab.com/ayedocloudsolutions/lifecycle/compare/v1.0.0...v1.0.1) (2021-05-10)


### Bug Fixes

* corrected variable name ([d1b41f9](https://gitlab.com/ayedocloudsolutions/lifecycle/commit/d1b41f9ba5268ba9f19472d0be335a4d7ab00abb))

# 1.0.0 (2021-05-10)


### Features

* added Docker CLI ([9164471](https://gitlab.com/ayedocloudsolutions/lifecycle/commit/9164471ea99eff22f6335f63dc5d9ce9ca304103))
* added Docker CLI ([c8517bd](https://gitlab.com/ayedocloudsolutions/lifecycle/commit/c8517bd9c2ba2655c24d947e8fd38210ddc1af1f))
* added Docker CLI ([4d83a96](https://gitlab.com/ayedocloudsolutions/lifecycle/commit/4d83a96f43c26249ddaff7ea6770f68e00eb6011))
* added Docker CLI ([049650e](https://gitlab.com/ayedocloudsolutions/lifecycle/commit/049650e658a6be7275ed85aaff102784d3fc2723))
* added lifecycle cli ([86efb0c](https://gitlab.com/ayedocloudsolutions/lifecycle/commit/86efb0ccbfecf6c99e6f690cbd3d6f3322e13eff))
