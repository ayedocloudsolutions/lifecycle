# syntax = docker/dockerfile:experimental
ARG DOCKER_VERSION=20.10.6

FROM docker:${DOCKER_VERSION} AS docker-cli

FROM node:13

RUN mkdir -p /lifecycle \
    && apt-get update \
    && apt-get -y --no-install-recommends curl git install python3 python3-pip bash ca-certificates \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

COPY --from=docker-cli /usr/local/bin/docker /usr/local/bin/docker

COPY scripts/lifecycle.sh /usr/local/bin/lifecycle

RUN chmod +x /usr/local/bin/lifecycle \
    && /usr/local/bin/lifecycle gitlab-deps \
    && /usr/local/bin/lifecycle azure-deps \
    && /usr/local/bin/lifecycle docs-deps \
    && /usr/local/bin/lifecycle porter-deps

WORKDIR /lifecycle

COPY .releaserc.yml .releaserc.yml

COPY docker-entrypoint.sh /usr/local/bin/

RUN chmod +x /usr/local/bin/docker-entrypoint.sh \
    && groupadd -g 1000 docker \
    && useradd -g docker -u 1000 docker

ENTRYPOINT ["docker-entrypoint.sh"]

CMD ["sh"]

ARG BUILD_DATE
ARG VCS_REF
ARG BUILD_VERSION

LABEL org.label-schema.schema-version="1.0"
LABEL org.label-schema.build-date=$BUILD_DATE
LABEL org.label-schema.name="registry.gitlab.com/ayedocloudsolutions/lifecycle"
LABEL org.label-schema.description="a utility container for the software lifecycle"
LABEL org.label-schema.url="https://www.ayedo.de/"
LABEL org.label-schema.vcs-url="https://gitlab.com/ayedocloudsolutions/lifecycle"
LABEL org.label-schema.vcs-ref=$VCS_REF
LABEL org.label-schema.vcs-type="Git"
LABEL org.label-schema.vendor="ayedo Cloud Solutions"
LABEL org.label-schema.version=$BUILD_VERSION
LABEL org.label-schema.docker.dockerfile="/Dockerfile"