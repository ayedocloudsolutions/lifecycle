#!/usr/bin/env bash
set -o errexit
set -o pipefail
set -o nounset

# Globals
IFS=$'\t\n'
__DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
__FILE="${__DIR}/$(basename "${BASH_SOURCE[0]}")"
__BASE="$(basename ${__FILE} .sh)"
__ROOT="$(cd "$(dirname "${__DIR}")" && pwd)"
__DATE=$(date -u +'%Y-%m-%dT%H:%M:%SZ')
__VERSION=$(cat VERSION.txt || echo "build-$CI_PIPELINE_ID")
__DOCKER_IMAGE=$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG
__DOCKER_TAG=$CI_COMMIT_SHA
__DOCKER_REGISTRY_URL=$CI_REGISTRY
__DOCKER_REGISTRY_USER=gitlab-ci-token
__DOCKER_REGISTRY_PASSWORD=$CI_JOB_TOKEN
__PORTER_BUNDLE_NAME=${__ROOT}

debug() {
  echo "debug"
}

gitlab-deps() {
  npm install @semantic-release/exec @semantic-release/git @semantic-release/gitlab @semantic-release/changelog -D
}

azure-deps() {
  npm install @semantic-release/exec @semantic-release/git semantic-release-ado @semantic-release/changelog -D
}

docs-deps() {
  pip3 install mkdocs-material
}

docker-login() {
  echo "${DOCKER_REGISTRY_PASSWORD:-$__DOCKER_REGISTRY_PASSWORD}" | docker login ${DOCKER_REGISTRY_URL:-$__DOCKER_REGISTRY_URL} --username=${DOCKER_REGISTRY_USER:-$__DOCKER_REGISTRY_USER} --password-stdin
}

docker-build() {
  #docker buildx build --platform linux/amd64 -t ${DOCKER_IMAGE:-$__DOCKER_IMAGE}:${DOCKER_TAG:-$__DOCKER_TAG} --build-arg BUILD_DATE=${__DATE} --build-arg BUILD_VERSION=${VERSION:-$__VERSION} -f Dockerfile .
  docker build -t ${DOCKER_IMAGE:-$__DOCKER_IMAGE}:${DOCKER_TAG:-$__DOCKER_TAG} --build-arg BUILD_DATE=${__DATE} --build-arg BUILD_VERSION=${VERSION:-$__VERSION} -f Dockerfile .
}

docker-tag-latest() {
  docker tag ${DOCKER_IMAGE:-$__DOCKER_IMAGE}:${DOCKER_TAG:-$__DOCKER_TAG} ${DOCKER_IMAGE:-$__DOCKER_IMAGE}:latest
  docker push ${DOCKER_IMAGE:-$__DOCKER_IMAGE}:latest
}

docker-tag-version() {
  docker tag ${DOCKER_IMAGE:-$__DOCKER_IMAGE}:${DOCKER_TAG:-$__DOCKER_TAG} ${DOCKER_IMAGE:-$__DOCKER_IMAGE}:${VERSION:-$__VERSION}
  docker push ${DOCKER_IMAGE:-$__DOCKER_IMAGE}:${VERSION:-$__VERSION}
}

porter-deps() {
  curl -L https://cdn.porter.sh/latest/install-linux.sh | bash
}

porter-update-version() {
  echo "${VERSION:-$__VERSION}" || exit 1
  sed -i -r "s|[[:digit:]]+\.[[:digit:]]+\.[[:digit:]]+|${VERSION:-$__VERSION}|g" porter.yaml
}

porter-build() {
  ${HOME}/.porter/porter build --name ${PORTER_BUNDLE_NAME:-$__PORTER_BUNDLE_NAME} --version ${VERSION:-$__VERSION}
}

porter-publish() {
  ${HOME}/.porter/porter publish
}

version() {
  npx semantic-release --generate-notes false --dry-run
}

release() {
  npx semantic-release
}

"$@"